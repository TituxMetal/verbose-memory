<?php

// Parse the url and show if file exists in public directory
$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
if($url != '/' && file_exists(__DIR__ . '/public' . $url)) {
	return false;
}

// Redifine the script name to pointing on the index.php main file
$_SERVER['SCRIPT_NAME'] = '/index.php';

require_once __DIR__ . '/public/index.php';