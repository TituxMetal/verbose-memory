<?php

// Define some constants
define('ROOT', dirname(__DIR__));
define('DS', '/');
define('CONFIG_PATH', ROOT . DS . 'config/app.json');

// Require the autoloader from composer
require ROOT . '/vendor/autoload.php';